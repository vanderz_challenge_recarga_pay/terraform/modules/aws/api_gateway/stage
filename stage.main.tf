resource "aws_api_gateway_deployment" "main" {
  rest_api_id = var.api_id
  stage_name  = var.deployment_stage_name

  lifecycle {
    create_before_destroy = true
  }

  triggers = var.deployment_triggers
  variables = var.deployment_variables
}

resource "aws_api_gateway_stage" "main" {
  stage_name    = var.stage_name
  rest_api_id   = var.api_id
  deployment_id = aws_api_gateway_deployment.main.id
  cache_cluster_enabled = var.cache_cluster_enabled
  cache_cluster_size = var.cache_cluster_size
  variables = var.stage_variables
  tags = var.stage_tags

  lifecycle {
    ignore_changes = [ deployment_id, ]
  }
}