variable "api_id" {}

variable "deployment_triggers" {}

variable "deployment_variables" {}

variable "deployment_stage_name" {}

variable "stage_name" {}

variable "stage_tags" {}

variable "stage_variables" {}

variable "cache_cluster_enabled" {}

variable "cache_cluster_size" {}

